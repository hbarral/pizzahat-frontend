# build stage
FROM node:14.21.3-alpine3.16 as build-stage

ARG API_URL_DEVELOPMENT
ENV API_URL_DEVELOPMENT=$API_URL_DEVELOPMENT

ARG API_URL_PRODUCTION
ENV API_URL_PRODUCTION=$API_URL_PRODUCTION

ARG STRIPE_KEY
ENV STRIPE_KEY=$STRIPE_KEY

WORKDIR /pizzahat
COPY package*.json /pizzahat/
RUN apk update
 # && apk add yarn

# RUN node --max-old-space-size=250 `which npm` install --global yarn
RUN node --max-old-space-size=250 `which npm` install

# RUN node --max-old-space-size=250 `which yarn`
# RUN node --max-old-space-size=250 yarn
# RUN yarn

COPY . /pizzahat/
# RUN yarn prod
RUN node --max-old-space-size=250 `which npm` run prod

# production stage
FROM nginx:1.17.9-alpine as production-stage
COPY --from=build-stage /pizzahat/dist /usr/share/nginx/html
COPY --from=build-stage /pizzahat/dist /home/admin/web/pizzahat/public_html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

