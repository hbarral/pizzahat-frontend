const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { VueLoaderPlugin } = require('vue-loader')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const DotenvPlugin = require('webpack-dotenv-plugin')
const dotenv = require('dotenv')
dotenv.config({
  path: path.resolve(__dirname, '../.env')
})
const cssnano = require('cssnano')
const purgecss = require('@fullhuman/postcss-purgecss')

const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  entry: {
    'app': ['@babel/polyfill', path.resolve(__dirname, '../resources/app.js')]
  },
  output: {
    filename: devMode ? 'js/[name].js' : 'js/[name].[hash].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  resolve: {
    mainFiles: ['index'],
    alias: {
      '@': path.resolve(__dirname, '../resources'),
      vue$: 'vue/dist/vue.runtime.esm.js'
    },
    extensions: [
      '.mjs',
      '.js',
      '.jsx',
      '.vue',
      '.json',
      '.wasm'
    ]
  },
  devServer: {
    contentBase: path.resolve(__dirname, '../public'),
    index: 'index.html',
    port: 9000,
    stats: 'minimal', // 'none' | 'errors-only' | 'minimal' | 'normal' | 'verbose'
    overlay: true
    // clientLogLevel: 'silent',
    // noInfo: true,
    // quiet: true,
    // onListening(server) {
    // const port = server.listeningApp.address().port
    // console.log('Listening on port:', port)
    // }
  },
  optimization: {
    minimize: true,
    minimizer: [
      new CssMinimizerPlugin({
        minimizerOptions: {
          preset: [
            'default',
            {
              discardComments: { removeAll: true }
            }
          ]
        }
      }),
      new TerserPlugin({
        terserOptions: {
          mangle: true,
          keep_classnames: false,
          keep_fnames: false,
          output: {
            comments: false
          }
        },
        extractComments: false
      })
    ]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  require('tailwindcss')(path.resolve(__dirname, '../tailwind.config.js')),
                  !devMode ? require('autoprefixer') : null,
                  !devMode
                    ? cssnano({ preset: 'default' })
                    : null,
                  purgecss({
                    content: ['./resources/**/*.html', './resources/**/*.vue', './resources/**/*.jsx'],
                    defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
                  })
                ]
              }
            }
          }
        ]
      },
      {
        test: /\.vue$/,
        use: 'vue-loader'
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin({
      dry: false,
      cleanOnceBeforeBuildPatterns: [
        path.resolve(__dirname, '../dist')
      ],
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),
    new HtmlWebpackPlugin({
      showErrors: true,
      cache: true,
      filename: 'index.html',
      chunks: ['app'],
      title: 'Pizza Hat',
      description: 'Pizza Hat',
      template: path.resolve(__dirname, '../resources/index.html'),
      inject: true
    }),
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: devMode ? 'css/[name].css' : 'css/[name].[hash].css',
      chunkFilename: devMode ? '[id].css' : '[id].[hash].css'
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, '../resources/assets/images/'),
          to: path.resolve(__dirname, '../public/assets/images/')
        },
        {
          from: path.resolve(__dirname, '../resources/assets/images/'),
          to: path.resolve(__dirname, '../dist/assets/images/')
        },
        {
          from: path.resolve(__dirname, '../resources/assets/images/favicon.ico'),
          to: path.resolve(__dirname, '../public/')
        },
        {
          from: path.resolve(__dirname, '../resources/assets/images/favicon.ico'),
          to: path.resolve(__dirname, '../dist/')
        }
      ]
    }),
    new DotenvPlugin({
      path: path.resolve(__dirname, '../.env'),
      sample: path.resolve(__dirname, '../.env.example'),
      allowEmptyValues: true
    })
  ]
}

