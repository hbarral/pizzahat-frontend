import request from '@/utils/request'

export function fetchProducts(category) {
  return request({
    url: `/api/products/${category}`,
    method: 'get'
  })
}
