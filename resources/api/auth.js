import request from '@/utils/request'

export function signIn(credentials) {
  return request({
    url: '/api/login',
    method: 'post',
    data: credentials
  })
}

export function fetchUser() {
  return request({
    url: '/api/me',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/api/logout',
    method: 'post'
  })
}
