import request from '@/utils/request'

export function login(data) {
  return request({
    url: data.url,
    method: 'post'
  })
}

export function getUser() {
  return request({
    url: '/api/user',
    method: 'get'
  })
}
