import request from '@/utils/request'

export function fetchOrders() {
  return request({
    url: '/api/orders/',
    method: 'get'
  })
}

export function sendOrder(order) {
  return request({
    url: '/api/orders/',
    method: 'post',
    data: order
  })
}

export function orderDetails(orderId) {
  return request({
    url: `/api/orders/${orderId}`,
    method: 'get'
  })
}
