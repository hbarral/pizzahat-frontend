import Vue from 'vue'
import VueRouter from 'vue-router'
import MenuContainer from '@/components/MenuContainer'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    {
      name: 'main',
      path: '/',
      component: MenuContainer,
      props: {
        category: 'pizzas'
      }
    },
    {
      name: 'pizzas',
      path: '/pizzas',
      component: MenuContainer,
      props: {
        category: 'pizzas'
      }
    },
    {
      name: 'drinks',
      path: '/drinks',
      component: () => import('@/components/MenuContainer'),
      props: {
        category: 'drinks'
      }
    },
    {
      name: 'login',
      path: '/login',
      component: () => import('@/components/Login'),
      meta: { }
    },
    {
      name: 'dashboard',
      path: '/dashboard',
      component: () => import('@/components/Dashboard'),
      meta: { }
    }
  ]
})
