import axios from 'axios'
import store from '@/store'

const service = axios.create({
  baseURL: (!process.env.NODE_ENV || process.env.NODE_ENV === 'development')
    ? process.env.API_URL_DEVELOPMENT
    : process.env.API_URL_PRODUCTION,
  timeout: 15000
})

service.interceptors.request.use(
  config => {
    config.headers.Accept = 'application/json'
    config.headers.ContentType = 'application/json'

    if (store.getters.token) {
      config.headers.Token = store.getters.token
      config.headers.Authorization = `Bearer ${store.getters.token}`
    }
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data

    return res
  },
  error => {
    return Promise.reject(error)
  }
)

export default service
