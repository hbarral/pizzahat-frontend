import Vue from 'vue'
import App from './App.vue'
import './styles/app.css'
import { library, dom } from '@fortawesome/fontawesome-svg-core'
import { faTrash, faPlus, faCogs, faArrowUp, faMinus, faTicketAlt, faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from '@/router'
import store from '@/store'
import Cookies from 'js-cookie'

if (store.getters.token) {
  store.dispatch('auth/fetchUser')
    .catch(() => {
      Cookies.remove('token')
    })
}

library.add(faTrash, faPlus, faCogs, faArrowUp, faMinus, faTicketAlt, faShoppingCart)
Vue.component('font-awesome-icon', FontAwesomeIcon)
dom.watch()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
