const getters = {
  token: state => state.auth.token,
  authenticated: state => state.auth.token && state.auth.user,
  user: state => state.auth.user,
  products: state => {
    return category => {
      return state.product.products[category]
    }
  },
  order: state => state.order.order,
  orders: state => state.order.orders,
  deliveryCost: (state, { total, totalItems }) => {
    if (totalItems && ((total / state.order.currentCurrency.factor) < state.order.order.delivery.freeAt)) {
      return state.order.order.delivery.cost * state.order.currentCurrency.factor
    } else {
      return 0
    }
  },
  totalItems: state => {
    const items = state.order.order.products.reduce((sum, curr) => {
      return sum + curr.quantity
    }, 0)
    return items
  },
  total: state => {
    const total = state.order.order.products.reduce((sum, curr) => {
      return sum + (curr.quantity * curr.price * state.order.currentCurrency.factor)
    }, 0)
    return total
  },
  currencies: state => state.order.currencies,
  currentCurrency: state => state.order.currentCurrency,
  currentStep: state => state.order.currentStep
}

export default getters
