import Cookies from 'js-cookie'
import { signIn, fetchUser, logout } from '@/api/auth'

const state = {
  token: Cookies.get('token'),
  user: null
}

const mutations = {
  SAVE_TOKEN: (state, { token, remember }) => {
    state.token = token
    Cookies.set('token', token, { expires: remember ? 365 : null })
  },
  FETCH_USER_SUCCESS: (state, { user }) => {
    state.user = user
  }
}

const actions = {
  async signIn({ commit }, credentials) {
    const response = await signIn(credentials)
    const { token, user } = response

    commit('SAVE_TOKEN', { token, remember: true })
    commit('FETCH_USER_SUCCESS', { user })
  },

  async fetchUser({ commit }) {
    const user = await fetchUser()
    commit('FETCH_USER_SUCCESS', { user })
  },

  async logout({ commit }) {
    const response = await logout()
    commit('SAVE_TOKEN', { token: null, remember: false })
    commit('FETCH_USER_SUCCESS', { user: null })
    Cookies.remove('token')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
