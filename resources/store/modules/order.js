import { fetchOrders, sendOrder, orderDetails } from '@/api/order'

const state = {
  currencies: [
    {
      name: 'Russian Ruble',
      symbol: '₽',
      image: '/assets/images/ruble.svg',
      factor: 75.69
    },
    {
      name: 'Euro',
      symbol: '€',
      image: '/assets/images/euro.svg',
      factor: 0.84
    },
    {
      name: 'US Dollar',
      symbol: '$',
      image: '/assets/images/us_dollar.svg',
      factor: 1
    }
  ],
  currentCurrency: {
    name: 'US Dollar',
    symbol: '$',
    image: '/assets/images/us_dollar.svg',
    factor: 1
  },
  orders: { },
  order: {
    products: [],
    delivery: {
      cost: 2.65,
      freeAt: 30
    },
    address: '',
    name: '',
    surname: '',
    orderIntentToken: ''
  },
  currentStep: 1
}

const mutations = {
  SET_ORDERS: (state, orders) => {
    state.orders = orders
  },
  ADD_ITEM: (state, item) => {
    state.order.products.push(item)
  },
  UPDATE_ITEM: (state, item) => {
    const foundItem = state.order.products.find(i => i.id === item.id)
    const index = state.order.products.findIndex(i => i.id === item.id)

    const newItem = {
      ...foundItem,
      ...item
    }

    state.order.products.splice(index, 1, newItem)
  },
  REMOVE_ITEM: (state, item) => {
    const foundItem = state.order.products.find(i => i.id === item.id)
    const index = state.order.products.findIndex(i => i.id === item.id)

    if (foundItem.quantity > 1) {
      foundItem.quantity--
      state.order.products.splice(index, 1, foundItem)
    } else {
      state.order.products.splice(index, 1)
    }
  },
  SET_CURRENCY: (state, currency) => {
    state.currentCurrency = currency
  },
  SET_STEP: (state, step) => {
    state.currentStep = step
  },
  SET_ADDRESS: (state, address) => {
    state.order.address = address
  },
  SET_NAME: (state, name) => {
    state.order.name = name
  },
  CLEAN_ORDER: state => {
    state.order.products = []
    state.order.address = ''
    state.order.name = ''
  },
  SET_ORDER_INTENT_TOKEN: (state, token) => {
    state.order.orderIntentToken = token
  }
}

const actions = {
  async fetchOrders({ commit }) {
    const orders = await fetchOrders()
    commit('SET_ORDERS', orders)
  },
  addToCart({ state, commit }, item) {
    const foundItem = state.order.products.find(i => i.id === item.id)

    if (foundItem) {
      foundItem.quantity++
      commit('UPDATE_ITEM', foundItem)
    } else {
      const newItem = Object.assign(item, { quantity: 1 })
      commit('ADD_ITEM', newItem)
    }
  },
  subtractFromCart({ state, commit }, item) {
    const foundItem = state.order.products.find(i => i.id === item.id)

    commit('REMOVE_ITEM', foundItem)
  },
  async sendOrder({ state, commit }) {
    const order = {
      products: [...state.order.products],
      name: state.order.name,
      surname: state.order.surname,
      address: state.order.address,
      orderIntentToken: state.order.orderIntentToken
    }

    const response = await sendOrder(order)

    return response
  },
  async orderDetails(context, orderId) {
    const details = await orderDetails(orderId)
    return details
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
