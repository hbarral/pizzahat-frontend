/* import Cookies from 'js-cookie' */
import { fetchProducts } from '@/api/product'

const state = {
  products: {
    pizzas: null,
    drinks: null
  }
}

const mutations = {
  SET_PRODUCTS: (state, payload) => {
    state.products[payload.category] = payload.products
  }
}

const actions = {
  async fetchProducts({ commit }, category) {
    const products = await fetchProducts(category)
    commit('SET_PRODUCTS', { category, products })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

