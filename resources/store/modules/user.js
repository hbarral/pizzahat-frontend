import Cookies from 'js-cookie'

const state = {
  user: null,
  name: '',
  avatar: '',
  introduction: '',
  roles: []
}

const mutations = {
  FETCH_USER_SUCCESS: (state, { user }) => {
    state.user = user
  },

  FETCH_USER_FAILURE: state => {
    state.token = null
    Cookies.remove('token')
  }
}

const actions = { }

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
